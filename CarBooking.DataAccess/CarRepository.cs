﻿using System;
using System.Collections.Generic;
using CarBooking.Common.Exceptions;
using CarBooking.Common.Models;

namespace CarBooking.DataAccess
{
    public class CarRepository : IRepository<Car>
    {
        //For simplicity, I create the dummy car lists here instead of getting it from database. 
        //When getting from database the result set would be IEnumerable. I use IDictionary here also for the sake of simplicity when I perform getById or Update. 
        private readonly IDictionary<int, Car> _dummyCarListsFromDB; 

        private static CarRepository _instance;

        public static CarRepository Instance => _instance ?? (_instance = new CarRepository());

        private CarRepository()
        {
            _dummyCarListsFromDB = new Dictionary<int, Car>();
            for (int i = 1; i <= 5; i++)
            {
                _dummyCarListsFromDB.Add(i, new Car(i, $"This is car No. {i}") {LastBookedAt = DateTime.MinValue});
            }
        }

        public IEnumerable<Car> GetAll()
        {
            return _dummyCarListsFromDB.Values;
        }

        public Car GetById(int id)
        {
            if (!_dummyCarListsFromDB.ContainsKey(id))
            {
                throw new IdNotFoundException($"The system cannot find a car with id {id}");
            }
            // I choose not to return _dummyCarListsFromDB[id] directly here because in reality this is a data item from DB that needs to be converted to Car. 
            // I don't want the change made to the returned Car instance immediately impacting the _dummyCarListsFromDB
            var dataItem = _dummyCarListsFromDB[id];    
            return new Car(dataItem.Id, dataItem.Description)
            {
                BookedBy = dataItem.BookedBy,
                LastBookedAt = dataItem.LastBookedAt
            };
        }

        public void Create(Car car)
        {
            throw new NotImplementedException();
        }

        public void Delete(Car car)
        {
            throw new NotImplementedException();
        }
        
        public void Update(Car car)
        {
            if (!_dummyCarListsFromDB.ContainsKey(car.Id))
            {
                throw new IdNotFoundException($"The system cannot find a car with id {car.Id}");
            }
            _dummyCarListsFromDB[car.Id] = car;
        }
    }
}
