﻿using System.Configuration;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json.Serialization;

namespace CarBookingAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            EnableCors(config);
            ConfigureJsonFormatter(config);
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        private static void ConfigureJsonFormatter(HttpConfiguration config)
        {
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(
                new Newtonsoft.Json.Converters.StringEnumConverter { CamelCaseText = true });
            config.Formatters.JsonFormatter.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Local;
        }

        private static void EnableCors(HttpConfiguration config)
        {
            var allowedOrigins = ConfigurationManager.AppSettings["CorsAllowedOrigins"];

            if (string.IsNullOrEmpty(allowedOrigins))
                return;

            var corsAttr = new EnableCorsAttribute(origins: allowedOrigins, headers: "*", methods: "GET, POST, PUT, DELETE, OPTIONS")
            {
                SupportsCredentials = true
            };

            config.EnableCors(corsAttr);
        }
    }
}
